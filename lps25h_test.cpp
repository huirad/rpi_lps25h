/**************************************************************************
 * @brief Test program for lps25h.cpp
 *
 * @copyright Copyright (C) 2016, Helmut Schmidt
 *
 * @license MPL-2.0 <http://spdx.org/licenses/MPL-2.0>
 *
 **************************************************************************/


#include "lps25h.h"
#include <stdio.h>
#include <unistd.h>

void lps25h_cb(const float pressure[], const float temperature[], const uint64_t timestamp[], const uint16_t num_elements)
{
    for (uint16_t i=0; i<num_elements; i++)
    {
        printf("%llu %hu P %+7.3f T %+5.2f\n",
            timestamp[i],
            i,
            pressure[i],
            temperature[i]
            );
    }
}


int main()
{
    bool result = false;
    float pressure;
    float temperature;
    uint64_t timestamp;
    uint64_t time_start;
    uint64_t time_end;
    int n_loops = 10;

    result = lps25h_init(LPS25H_I2C_DEV_DEFAULT, LPS25H_ADDR_1, LPS25H_ODR_25HZ, LPS25H_AVG_0);

    if (result)
    {
        result = true;
        time_start = lps25h_get_timestamp();
        for (int i=0; i<n_loops; i++)
        {
            result = result && lps25h_read(&pressure, &temperature, &timestamp);  
        }
        time_end = lps25h_get_timestamp();
        printf ("%d Loops: Start %llu End %llu\n", n_loops, time_start, time_end);
        if (result)
        {
            printf("Pressure: %+7.3f, Temperature: %+5.2f, Timestamp: %llu, Duration: %llu\n",
                pressure,
                temperature,
                timestamp,
                time_end-time_start
                );
        }
        else
        {
            printf("ERROR: Could not read LPS25H\n");
        }

      //Callback Test
        lps25h_register_callback(&lps25h_cb);
        lps25h_start_reader_thread(20, 5, false);
        sleep(10);
        lps25h_stop_reader_thread();
        lps25h_deregister_callback(&lps25h_cb);
    }
    else
    {
        printf("ERROR: Could not init LPS25H\n");
    }

    return 0;
}
